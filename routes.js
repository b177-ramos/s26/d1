let http = require("http");

// Creates a variable "port" to store the port numer
const port = 4000;

const server = http.createServer((request, response ) => {
	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type': 'textplain'})
		response.end('You are in /greeting endpoint');
	}
	else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type': 'textplain'})
		response.end('You are in /homepage endpoint');
	}
	else{
		response.writeHead(404, {'Content-Type': 'textplain'})
		response.end('Page Unavailable');
	}
});
	
server.listen(port);

console.log(`Server now accesible at localhost:${port}.`);

// npx kill-port 4000 -- terminate ng terminal