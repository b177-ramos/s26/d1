// REST API -- Understanding RESTful Services


// Imagine we are making a web application that aggregates and display real state agents in our area

// -- Different Read Estate companies: Company A, Company B, Company C, Company D

// -- Companies will provide API, application will use the API.

// -- API -- Application Programming Interface

// API example:
// Remote Control to Television -- Remote Control will be the interface. You presses buttons to execute commands/requests/tasks.

// -- Interface (endpoints) - Remote buttons to execute a specific commands/requests/tasks.

// Company A (https://www.companya.com) -- (https://www.companya.com/api/agents -- this is the API to get the company A's agents

// Our application will make a GET request to access those information through the API's // Application Get Request > API > Company Data // Then Company Responds > API > Application receives get request

// Once we get the requested data, we will make a User Interface for the users // Company >> API >> JSON (DataBase) >> Application

//REST API
// (RE)presentational (S)tate (T)ransfer
// Architectural style of the web
// A standard or set of guidelines which we can structure our APIs


// --------------------------------------------------------------------- //



// Client Server Architecture
// -- a computing model in which the server hosts, delivers and manages most of the resources and services to be consumed by the client. This type of architecture has one or more client computers connected to a central server over a network or internet connection
// -- client requests to the server, server returns an input

// Node.js Introduction


// Requiring the HTTP module
let http = require("http"); // lets transfer data from one application to another.


// Creating the server
// -- createServer() method
//created a server //network should listen to the '4000' specific port
http.createServer(function (request, response) {
	response.writeHead(200, {'Content-Type': 'text/plain'});

	response.end('Hello, World! from Batch 177');
} ).listen(4000);

// Create a console.log to indicate that the server is running.

console.log('Server runnng at localhost:4000');








